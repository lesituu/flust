## Flexible LUST ##

A modification of LUST that allows to modify the blending coefficient between the linear and linearUpwind scheme.In LUST the values is hardcoded to 0.75 towards the linear scheme.
See FLUST.H for an example for how to specify the coefficient.

**This offering is not approved or endorsed by OpenCFD Limited, producer and distributor of the OpenFOAM software via www.openfoam.com, and owner of the OPENFOAM� and OpenCFD� trademarks.**

## Compatibility ##

Developed with v1812 from OpenCFD, but is expected to work with any modern OpenFOAM.

## Installing ##

Clone and run wmake. Add libFLUST.so to libs() in your controlDict and you should be good to go.
